package quind.io;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SalaryCalculatorWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalaryCalculatorWebApplication.class, args);
	}

}

