package quind.io.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import quind.io.model.Constant;

@Repository
public interface ConstantRepository extends JpaRepository<Constant, Long> {
	 /*
	@Query("SELECT * FROM constant c WHERE c.name = :name") 
	 public Constant findByName(@Param("name") String name);
	
	@Query("UPDATE constant SET valor= ':valor' WHERE name= :name")
	public void updateByNombre(@Param("name") String name, @Param("valor") Float valor);
	*/
}