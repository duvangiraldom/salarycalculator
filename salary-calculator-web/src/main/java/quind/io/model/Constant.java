package quind.io.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "constants")
public class Constant{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", unique = true, nullable = false)
    private Long id;

	@Column(name="name", nullable = false, length=50)
    @Size(min = 1, max = 50)
    private String name;

	@Column(name="valor", nullable = false)
	private Double valor;

	public Constant() {}
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Constant(Long id, @Size(min = 1, max = 50) String name, Double valor) {
		super();
		this.id = id;
		this.name = name;
		this.valor = valor;
	}
   
}
    