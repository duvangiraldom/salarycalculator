package quind.io.model;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "employees")
@Access(AccessType.FIELD)
public class Employee {
    
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id", unique = true, nullable = false)
    private Long id;

	@Column(name="first_name", nullable=false, length=255)
    @Size(min = 1, max = 50)
    private String firstName;

	@Column(name="last_name", nullable=false, length=255)   
	@Size(max = 50)
    private String lastName;
    
	@Column(name="basic_salary", nullable=false)
	private Double basicSalary;
    
	@Column(name="is_colombian", nullable=false)
	private Boolean isColombian;
    
	@Column(name="is_student", nullable=false)
	private Boolean isStudent;
	
	@Column(name="final_salary", nullable=false)
	private Double finalSalary;

	public Long getId() {
		return id;
	}

	public Employee() {
		
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(Double basicSalary) {
		this.basicSalary = basicSalary;
	}

	public Boolean getIsColombian() {
		return isColombian;
	}

	public void setIsColombian(Boolean isColombian) {
		this.isColombian = isColombian;
	}

	public Boolean getIsStudent() {
		return isStudent;
	}

	public void setIsStudent(Boolean isStudent) {
		this.isStudent = isStudent;
	}

	public Double getFinalSalary() {
		return finalSalary;
	}

	public void setFinalSalary(Double finalSalary) {
		this.finalSalary = finalSalary;
	}

	public void validateNullCamps() {
		if(firstName == null) {
			firstName = "";
		}
		if(lastName == null) {
			lastName = "";
		}
		if(basicSalary == null) {
			basicSalary = 0.0;
		}
		if(isColombian == null) {
			isColombian = false;
		}
		if(isStudent == null) {
			isStudent = false;
		}
		if(finalSalary == null) {
			finalSalary = 0.0;
		}
		
	}

	public Employee(Long id, @Size(min = 1, max = 50) String firstName, @Size(max = 50) String lastName,
			Double basicSalary, Boolean isColombian, Boolean isStudent, Double finalSalary) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.basicSalary = basicSalary;
		this.isColombian = isColombian;
		this.isStudent = isStudent;
		this.finalSalary = finalSalary;
	}
	
	

}