package quind.io.util;

import io.quind.Calculator;
import quind.io.model.Constant;
import quind.io.model.Employee;

public class SalaryCalculator {
		
	public Double calculeValueToSalaryPaid(Employee employee){
		
		Constant transportAssistance = new Constant((long)1,"transportAssistance", 100.0);
		Constant taxStudentForeign = new Constant((long)2,"taxStudentForeign", 0.1);
		Calculator calculator= new Calculator();
		Double valueToPaid = employee.getBasicSalary();
		
		if(employee.getIsStudent()) {
			valueToPaid = calculator.sumBetweenTwoNumbers(valueToPaid, transportAssistance.getValor());
		}
		
		if(employee.getIsStudent() && !employee.getIsColombian()) {
			Double percentageTaxStudentForeign = calculator.multiplicationBetweenTwoNumbers(valueToPaid, taxStudentForeign.getValor()); 
			valueToPaid = calculator.sumBetweenTwoNumbers(valueToPaid, percentageTaxStudentForeign);
		}
		
		
		if(!employee.getIsColombian()) {
			Double stablePercentage = calculator.multiplicationBetweenTwoNumbers(valueToPaid, 0.15);
			valueToPaid = calculator.restBetweenTwoNumbers(valueToPaid, stablePercentage);
		}
		
		return valueToPaid;
	}

}

