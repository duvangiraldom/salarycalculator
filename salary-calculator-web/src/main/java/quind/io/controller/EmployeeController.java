package quind.io.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import quind.io.dao.impl.ConstantDAO;
import quind.io.dao.impl.EmployeeDAO;
import quind.io.model.Employee;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired(required = true)
	protected EmployeeDAO employeeDAO;
	
	@Autowired(required = true)
	protected ConstantDAO constantDAO;
	
	private ObjectMapper mapper;
	
	@RequestMapping(value="/listAll", method= RequestMethod.GET)
	public List<Employee> listAll() {
		return employeeDAO.findAll();
	}
	
	@RequestMapping(value="/search/{id}", method= RequestMethod.GET)
	public Employee show(@PathVariable("id") long id) {
		return this.employeeDAO.findById(id);
	}
	
	@RequestMapping(value="/delete/{id}", method= RequestMethod.DELETE)
	public void delete(@PathVariable("id") long id) {
		this.employeeDAO.deleteById(id);
	}
	
	@RequestMapping(value="/create", method= RequestMethod.POST)
	public void create(@RequestBody String EmployeeJson) throws JsonParseException, JsonMappingException, IOException{
		this.mapper = new ObjectMapper();
		this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		Employee employee = this.mapper.readValue(EmployeeJson, Employee.class);				
		employeeDAO.save(employee);
	}

	@RequestMapping(value="/update/{id}", method= RequestMethod.PUT)
	public void update(@PathVariable("id") long id, @RequestBody String EmployeeJson) throws JsonParseException, JsonMappingException, IOException{
		this.mapper = new ObjectMapper();
		this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		Employee employee = this.mapper.readValue(EmployeeJson, Employee.class);
		employee.setId(id);		
		this.employeeDAO.save(employee);	
	}
}
