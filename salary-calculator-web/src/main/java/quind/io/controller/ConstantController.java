package quind.io.controller;


import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import quind.io.dao.impl.ConstantDAO;
import quind.io.model.Constant;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/constants")
public class ConstantController {
	
	@Autowired(required = true)
	protected ConstantDAO constantDAO;
	
	private ObjectMapper mapper;
	
	@RequestMapping(value="/listAll", method= RequestMethod.GET)
	public List<Constant> listAll() {
		return this.constantDAO.findAll();
	}
	
	@RequestMapping(value="/search/{id}", method= RequestMethod.GET)
	public Constant show(@PathVariable("id") long id) {
		return constantDAO.findById(id);
	}
	
	@RequestMapping(value="/delete/{id}", method= RequestMethod.DELETE)
	public void delete(@PathVariable("id") long id) {
		this.constantDAO.deleteById(id);
	}
	
	@RequestMapping(value="/create", method= RequestMethod.POST)
	public void create(@RequestBody String ConstantJson) throws JsonParseException, JsonMappingException, IOException{
		this.mapper = new ObjectMapper();
		this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		Constant constant = this.mapper.readValue(ConstantJson, Constant.class);
		
		constantDAO.save(constant);
	}
	
	@RequestMapping(value="/update/{id}", method= RequestMethod.PUT)
	public void update(@PathVariable("id") long id, @RequestBody String ConstantJson) throws JsonParseException, JsonMappingException, IOException{
		this.mapper = new ObjectMapper();
		this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		Constant constant = this.mapper.readValue(ConstantJson, Constant.class);
		constant.setId(id);
		
		constantDAO.save(constant);
		
	}
}
