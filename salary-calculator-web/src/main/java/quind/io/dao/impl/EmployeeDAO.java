package quind.io.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import quind.io.dao.IEmployeeDAO;
import quind.io.model.Employee;
import quind.io.util.SalaryCalculator;

@Component
public class EmployeeDAO {
		
	@Autowired
	IEmployeeDAO iEmployeeDAO;
		
	public Employee save(Employee employee) {	
		employee.validateNullCamps();
		SalaryCalculator salaryCalculator = new SalaryCalculator();
		Double finalSalary = salaryCalculator.calculeValueToSalaryPaid(employee);
		employee.setFinalSalary(finalSalary);
		return iEmployeeDAO.save(employee);
	}
	
	public List<Employee> findAll() {
		return iEmployeeDAO.findAll();
	}
	
	public Employee findById(long id) {
		return this.iEmployeeDAO.findById(id);
	}
	public void deleteById(long id) {
		this.iEmployeeDAO.deleteById(id);
	}
}
