package quind.io.dao.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import quind.io.dao.IConstantDAO;
import quind.io.model.Constant;

@Component
public class ConstantDAO {
	
	@Autowired
	IConstantDAO iConstantDAO;
	
	public Constant save(Constant constant) {	
		return iConstantDAO.save(constant);
	}
	
	public List<Constant> findAll() {
		return this.iConstantDAO.findAll();
	}
	
	public Constant findById(long id) {
		return this.iConstantDAO.findById(id);
	}
	public void deleteById(long id) {
		this.iConstantDAO.deleteById(id);
	}
}
