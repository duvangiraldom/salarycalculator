package quind.io.dao;

import java.util.List;

import quind.io.model.Constant;
import quind.io.repository.ConstantRepository;

public interface IConstantDAO extends ConstantRepository{
	@SuppressWarnings("unchecked")
	Constant save(Constant constant);
	
	List<Constant> findAll();
	
	Constant findById(long id);
	
	void deleteById(long id);
}
