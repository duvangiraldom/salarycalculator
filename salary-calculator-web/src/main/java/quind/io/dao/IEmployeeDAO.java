package quind.io.dao;
import java.util.List;

import quind.io.model.Employee;
import quind.io.repository.EmployeeRepository;

public interface IEmployeeDAO extends EmployeeRepository{
	
	@SuppressWarnings("unchecked")
	Employee save(Employee employee);
	
	List<Employee> findAll();
	
	Employee findById (long id);
	
	void deleteById (long id);
	
}
