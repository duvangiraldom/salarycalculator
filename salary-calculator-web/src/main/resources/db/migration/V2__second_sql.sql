CREATE TABLE constants(
  id bigint(20) primary key auto_increment,
  name varchar(50) NOT NULL,
  valor DOUBLE PRECISION(10,4) null DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;