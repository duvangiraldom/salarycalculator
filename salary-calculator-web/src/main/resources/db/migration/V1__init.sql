CREATE TABLE employees(
  id bigint(20) primary key auto_increment,
  first_name varchar(255) not null,
  last_name varchar(255) not null,
  basic_salary DOUBLE PRECISION(10,4) not null,
  is_colombian BOOLEAN not NULL DEFAULT 0,
  is_Student BOOLEAN NULL DEFAULT 0,
  final_salary DOUBLE PRECISION(10,4) null DEFAULT 0.0000
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


