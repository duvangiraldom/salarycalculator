package quind.io.util;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;

//import quind.io.dao.IConstantDAO;
import quind.io.model.Employee;

public class SalaryCalculatorTest {

	//private IConstantDAO iConstantDAO = mock(IConstantDAO.class);
	SalaryCalculator salaryCalculator = new SalaryCalculator();
	private Employee employee = mock(Employee.class);
	
	@Test
	public void calculeValueToSalaryPaidToColombianPerson() {
		Double valueToPaidExpected = (double) 100;
		when(employee.getBasicSalary()).thenReturn((double) 100);
		when(employee.getIsColombian()).thenReturn(true);
		when(employee.getIsStudent()).thenReturn(false);
		
		Double valueToPaid = salaryCalculator.calculeValueToSalaryPaid(employee);
		Assert.assertEquals(valueToPaidExpected, valueToPaid);
	}
	
	@Test
	public void calculeValueToSalaryPaidToForeignPerson() {
		Double valueToPaidExpected = (double) 85;
		when(employee.getBasicSalary()).thenReturn((double) 100);
		when(employee.getIsColombian()).thenReturn(false);
		when(employee.getIsStudent()).thenReturn(false);
		
		Double valueToPaid = salaryCalculator.calculeValueToSalaryPaid(employee);
		Assert.assertEquals(valueToPaidExpected, valueToPaid);
	}
	
	@Test
	public void calculeValueToSalaryPaidToStudentPerson() {
		Double valueToPaidExpected = (double) 200;
		when(employee.getBasicSalary()).thenReturn((double) 100);
		when(employee.getIsColombian()).thenReturn(true);
		when(employee.getIsStudent()).thenReturn(true);
		
		Double valueToPaid = salaryCalculator.calculeValueToSalaryPaid(employee);
		Assert.assertEquals(valueToPaidExpected, valueToPaid);
	}
	
	@Test
	public void calculeValueToSalaryPaidToStudentAndForeignPerson() {
		Double valueToPaidExpected = (double) 187;
		when(employee.getBasicSalary()).thenReturn((double) 100);
		when(employee.getIsColombian()).thenReturn(false);
		when(employee.getIsStudent()).thenReturn(true);
		
		Double valueToPaid = salaryCalculator.calculeValueToSalaryPaid(employee);
		Assert.assertEquals(valueToPaidExpected, valueToPaid);
	}
	
}
