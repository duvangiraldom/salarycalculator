package io.quind;

import org.junit.Assert;
import org.junit.Test;

import io.quind.Calculator;

public class CalculatorTest {

	private Calculator calculator = new Calculator();
	
	@Test
	public void testingSumBetweenTwoNumbers() {
		Double num1 = 5.0;
		Double num2 = 20.0;
		Double result = 25.0;

		Double sumBetweenTwoNumbers = calculator.sumBetweenTwoNumbers(num1, num2);
		
		Assert.assertEquals(result, sumBetweenTwoNumbers);
	}
	
	@Test
	public void testingRestBetweenTwoNumbers() {
		Double num1 = 14.0;
		Double num2 = 10.0;
		Double result = 4.0;
		
		Double restBetweenTwoNumbers = calculator.restBetweenTwoNumbers(num1, num2);
		
		Assert.assertEquals(result, restBetweenTwoNumbers);
	}
	
	@Test
	public void testingMultiplicationMBetweenTwoNumbers() {
		Double num1 = 4.0;
		Double num2 = 6.0;
		Double result = 24.0;
		
		Double multiplicationBetweenTwoNumbers = calculator.multiplicationBetweenTwoNumbers(num1, num2);
		
		Assert.assertEquals(result, multiplicationBetweenTwoNumbers);
	}
	
	@Test
	public void testingDivisionBetweenTwoNumbers() throws Exception {
		Double num1 = 9.0;
		Double num2 = 3.0;
		float despise = 0.001f;
		float result = 3f;

		try {
			Double divisionBetweenTwoNumbers = calculator.divisionBetweenTwoNumbers(num1, num2);
			Assert.assertEquals(result, divisionBetweenTwoNumbers, despise);
		}
		catch (Exception errorToDivide) {
			System.out.println(errorToDivide.getMessage());
		}
	}
	
	@Test(expected = Exception.class)
	public void testingDivisionByZero() throws Exception {
		Double num1 = 9.0;
		Double num2 = 0.0;
		
		calculator.divisionBetweenTwoNumbers(num1, num2);
	}
	
}
