package io.quind;

public class Calculator {

	public Double sumBetweenTwoNumbers(Double num1, Double num2) {
		return num1 + num2;
	}

	public Double restBetweenTwoNumbers(Double num1, Double num2) {
		return num1 - num2;
	}

	public Double multiplicationBetweenTwoNumbers(Double num1, Double num2) {
		return num1 * num2;
	}

	public Double divisionBetweenTwoNumbers(Double num1, Double num2) throws Exception {
		if (num2 != 0) {
			return num1 / num2;
		}
		else {
			throw new Exception("Error to divide number by zero");
		}
	}

}
